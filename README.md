# PrestaShop Classic Theme with hot module replacement - for webpack 2.x

## Getting started
### 1) Clone this repository:
```bash
# go to themes folder
cd ./themes

# clone theme
git clone https://github.com/Thomas-lhuillier/prestashop-1.7-webpack-2-hot-module-replacement ./classic-HMR
```

### 2) Install dependencies:
```bash
cd ./classic-HMR/_dev
npm install
# or
yarn
```

### 3) Configure paths:

  3.1) Open file: `_dev/hot.webpack.js`

  3.2) Check `themeFolderName` and `webpackConfig.output.publicPath`

### 4) Start hot mode:
```bash
# Hot mode:
npm run hot
# or
yarn hot
```

#### Other
Forked from:
https://github.com/retyui/prestashop-1.7-webpack-2-hot-module-replacement
